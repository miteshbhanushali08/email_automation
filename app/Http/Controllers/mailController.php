<?php

namespace App\Http\Controllers;

use App\Mail\sendEmail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class mailController extends Controller
{
    public function sendEmail(Request $request): \Illuminate\Http\JsonResponse
    {
        try {
            $request->validate([
                'name' => 'required',
                'email' => 'required|email',
                'messages' => 'required',
            ]);

            $data = [
                'name' => $request->input('name'),
                'email' => $request->input('email'),
                'messages' => $request->input('messages'),
            ];

            Mail::to($request->email)->send(new sendEmail($data));
             return response()->json(['message' => 'Email sent successfully'], 200);
        }
        catch (\Exception $exception){
            return response()->json(['message' => $exception->getMessage()], 500);
        }

    }
}
